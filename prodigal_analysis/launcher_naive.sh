#!/bin/bash -l

#OARSUB="oarsub --notify "mail:shaman.narayanasamy@uni.lu" -t bigsmp -t idempotent -t besteffort -l core=12/nodes=1,walltime=120"
OARSUB="oarsub --notify "mail:shaman.narayanasamy@uni.lu" -t bigmem -l core=2/nodes=1,walltime=72"

declare -a SAMPLES=("SM" "HF1" "HF2" "HF3" "HF4" "HF5" "WW1" "WW2" "WW3" "WW4" "BG")

### Repeat for all the data sets
for S in "${SAMPLES[@]}" 
do
    NAIVE_REF="/scratch/users/snarayanasamy/IMP_MS_data/naive_assemblies/${S}/MG_MT_cap3.merged.fa"

    OUTDIR="/scratch/users/snarayanasamy/IMP_MS_data/prodigal_analysis/naive_assemblies/${S}"
    ${OARSUB} -n "${S}_naive_prodigal" "./makeGenePredictionsProdigal.sh $NAIVE_REF $OUTDIR"
done
