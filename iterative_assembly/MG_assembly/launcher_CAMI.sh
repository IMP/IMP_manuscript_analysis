#!/bin/bash -l

#OARSUB="oarsub --notify "mail:shaman.narayanasamy@uni.lu" -t bigmem -t idempotent -t besteffort -l core=24/nodes=1,walltime=120"

OARSUB="oarsub --notify "mail:shaman.narayanasamy@uni.lu" -t bigmem -p "network_address=\'gaia-183\'" --project "project_biocore" -l core=20/nodes=1,walltime=120"

#OARSUB=""
#
#oarsub --notify "mail:shaman.narayanasamy@uni.lu" -n "iterative_assm_HMP" -t bigsmp -t idempotent -t besteffort -l nodes=1/core=24,walltime=120 ./execution_X310763260.sh 
#
#oarsub --notify "mail:shaman.narayanasamy@uni.lu" -n "iterative_assm_A02" -t bigsmp -t idempotent -t besteffort -l nodes=1/core=24,walltime=120 ./execution_A02.sh
#
#oarsub --notify "mail:shaman.narayanasamy@uni.lu" -n "simDat-iterative_assm" -t bigsmp -t idempotent -t besteffort -l nodes=1/core=24,walltime=120 ./execution_simDat.sh
#


#declare -a SAMPLES=("CAMI_low" "CAMI_medium" "CAMI_high")
declare -a SAMPLES=("CAMI_medium" "CAMI_high")

### Repeat for all the data sets
for S in "${SAMPLES[@]}" 
do
    INDIR="/scratch/users/snarayanasamy/IMP_MS_data/IMP_analysis/CAMI/${S}-idba/Preprocessing/"
    OUTDIR="/scratch/users/snarayanasamy/IMP_MS_data/iterative_assemblies/CAMI/MG_assemblies/${S}-iterative_MG"
    OUTLOG="/scratch/users/snarayanasamy/IMP_MS_data/iterative_assemblies/CAMI/MG_assemblies/${S}-iterative_MG/${S}_iterative_MG.log"
    TMPDIR="/scratch/users/snarayanasamy/IMP_MS_data/iterative_assemblies/CAMI/MG_assemblies/${S}-iterative_MG/tmp"
    
    ${OARSUB} -n "${S}_MG_iterative" "./execution_CAMI.sh $INDIR $OUTDIR $OUTLOG $TMPDIR"
    #CMD="./execution.sh $INDIR $OUTDIR $OUTLOG $TMPDIR"
    
    #echo $CMD
    #exec ${CMD}
done

