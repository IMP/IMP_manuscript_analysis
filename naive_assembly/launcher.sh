#!/bin/bash -l

OARSUB="oarsub --notify "mail:shaman.narayanasamy@uni.lu" -t bigmem -t idempotent -t besteffort -l core=8/nodes=1,walltime=2"
#OARSUB="oarsub --notify "mail:shaman.narayanasamy@uni.lu" -t bigmem -p "network_address=\'gaia-183\'" --project "project_biocore" -l core=8/nodes=1,walltime=120"

declare -a SAMPLES=("SM" "HF1" "HF2" "HF3" "HF4" "HF5" "WW1" "WW2" "WW3" "WW4" "BG")

### Repeat for all the data sets
for S in "${SAMPLES[@]}" 
do
    INDIR="/scratch/users/snarayanasamy/IMP_MS_data/IMP_analysis/${S}/Preprocessing/"
    OUTDIR="/scratch/users/snarayanasamy/IMP_MS_data/naive_assemblies/${S}"
    OUTLOG="/scratch/users/snarayanasamy/IMP_MS_data/naive_assemblies/${S}/${S}_naive_assembly.log"
    MG_ASSM="/scratch/users/snarayanasamy/IMP_MS_data/iterative_assemblies/MG_assemblies/${S}/MG_contigs_1.fa"
    MT_ASSM="/scratch/users/snarayanasamy/IMP_MS_data/iterative_assemblies/MT_assemblies/${S}/MT_contigs_1.fa"
    TMPDIR="/scratch/users/snarayanasamy/IMP_MS_data/naive_assemblies/${S}/tmp"
    
    #${OARSUB} -n "${S}_naive_assm" "./execution.sh $INDIR $OUTDIR $OUTLOG $MG_ASSM $MT_ASSM $TMPDIR"
    CMD="./execution.sh $INDIR $OUTDIR $OUTLOG $MG_ASSM $MT_ASSM $TMPDIR"

    echo $CMD
#    exec ${CMD}

done
