#!/bin/bash -l

source ./preload_modules.sh
INDIR=$1
OUTDIR=$2
OUTLOG=$3
MG_ASSM=$4
MT_ASSM=$5
TMPDIR=$6

date
### Initialize first assembly ###
INPUT_DIR=$INDIR \
  OUT_DIR=$OUTDIR \
  OUT_LOG=$OUTLOG \
  MG_ASSM=$MG_ASSM \
  MT_ASSM=$MT_ASSM \
  TMP=$TMPDIR \
  snakemake $7
date 
