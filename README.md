This repository contains the commands and scripts used for additional
analyses performed for the Integrated Meta-omic Pipelines (IMP) manuscript.

There are five sub-directories within this repository:
1. metAmos_commands: Containing MetAMOS execution commands
2. IMP_commands: Commands for running IMP
3. MOCAT_analysis Commands for running MOCAT
4. metaquast_analysis: The commands for running metaQUAST
5. iterative_assembly: Scripts and execution for the MG and MT iterative assembly
6. additional_analyses: Additional sub-directories containing scripts for data 
   extraction, summary and visualizations. Descriptions available inside each of the directories
7. prodigal_analysis: Some additional analyses performed with prodigal
